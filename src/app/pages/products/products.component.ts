import { Component } from '@angular/core';

@Component({
  selector: 'app-games',
  template: ` <router-outlet> </router-outlet> `,
})
export class ProductsComponent {}

//we need this file because it is the base route that houses all the children

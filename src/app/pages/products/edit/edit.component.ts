import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { ProductsService } from '../products.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit, OnDestroy {
  subs = new Subscription(); //this.subs.add()
  productForm = new FormGroup({
    productName: new FormControl(''),
    price: new FormControl(''),
    productDescription: new FormControl(''),
    imageCover: new FormControl(''),
    images: new FormControl(''),
  });

  constructor(
    private products: ProductsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .pipe(switchMap((params) => this.products.getOne(params.get('id')))) //use switchMap to kill other request and take the last request
        .subscribe((product: any) => {
          {
            const { images, imageCover, ...productRest } = product; //you have to send the images seperatly to do so pass images and imageCover on thier own and then the rest of the data
            console.log(product);
            this.productForm.patchValue(productRest);
            // this.gameForm.patchValue(images);
            // this.gameForm.patchValue(imageCover);
          }
        })
    ); //patchValue to prefill the form
  }

  //when the form loads use getOne() to prefill the form
  onSubmit() {
    //when you submit updateOne()s
    this.products
      .update(this.route.snapshot.params.id, this.productForm.value) //snapshot will take id from url
      .subscribe(() => this.router.navigateByUrl('products'));
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}

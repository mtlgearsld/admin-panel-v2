import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// const { apiUrl } = environment; //so we dont repeat typing environment.api.....
const apiUrl = (path: string) => `http://localhost:3000/${path}`;

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private readonly http: HttpClient) {}
  all() {
    return this.http.get<any[]>(apiUrl('products'));
  }

  getOne(id: string) {
    return this.http.get(apiUrl(`products/${id}`));
  }

  create(game: any) {
    return this.http.post(apiUrl(`products/`), game);
  }

  update(id: string, game: any) {
    return this.http.patch(apiUrl(`products/${id}`), game); //we need two params the id and the body
  }

  remove(id: string) {
    return this.http.delete(apiUrl(`products/${id}`)); //we need two params the id and the body
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateProductPostComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ProductsComponent } from './products.component';
import { ProductsListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    children: [
      { path: 'list', component: ProductsListComponent },
      { path: 'create', component: CreateProductPostComponent }, //do it like this for now so you can see where the routes are going but later ask bro how to do child routes
      { path: 'edit/:id', component: EditComponent }, //dont forget the COLON in the /:id!!!!!!
      { path: '', redirectTo: 'list', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule {}

// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'ngx-list',
//   templateUrl: './list.component.html',
//   styleUrls: ['./list.component.scss'],
// })
// export class ListComponent implements OnInit {
//   constructor() {}

//   ngOnInit() {}
// }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { mergeMap, map, switchMap, take } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';
@Component({
  selector: 'ngx-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  // selector: 'app-products',
  // templateUrl: './products.component.html',
  // styleUrls: ['./products.component.scss'],
})
export class ProductsListComponent {
  removedItems = new BehaviorSubject([]);

  removeItemFromSource = (id) =>
    this.removedItems.next([...this.removedItems.getValue(), id]);

  source = this.products

    .all()

    .pipe(
      mergeMap((data) =>
        this.removedItems.pipe(
          map((ids) => data.filter((d) => !ids.includes(d._id)))
        )
      )
    );

  settings = {
    mode: 'external',

    add: {
      addButtonContent: '<i class="nb-plus"></i>',

      createButtonContent: '<i class="nb-checkmark"></i>',

      cancelButtonContent: '<i class="nb-close"></i>',

      mode: 'external',
    },

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',

      saveButtonContent: '<i class="nb-checkmark"></i>',

      cancelButtonContent: '<i class="nb-close"></i>',

      mode: 'external',
    },

    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',

      confirmDelete: true,
    },

    columns: {
      // _id: {

      //   title: 'ID',

      //   type: 'number',

      //   editable: false

      // },

      productName: {
        title: 'Title',

        type: 'string',
      },

      price: {
        title: 'Year',

        type: 'number',
      },

      productDescription: {
        title: 'Description',

        type: 'string',
      },

      imageCover: {
        title: 'ImageCover',

        type: 'string',
      },

      images: {
        title: 'Images',

        type: 'string',
      },
    },
  };

  constructor(private products: ProductsService, private router: Router) {}

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.products

        .remove(event.data._id)

        .pipe(take(1))

        .subscribe(() => this.removeItemFromSource(event.data._id));
    }
  }

  onEdit(event): void {
    this.router.navigate(['/pages/products/edit', event.data._id]);
  }

  onCreate(event): any {
    this.router.navigate(['pages', 'products', 'create', event]);
  }
}

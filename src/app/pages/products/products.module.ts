import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { CreateProductPostComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsService } from './products.service';
import { ProductsListComponent } from './list/list.component';
import { ProductsComponent } from './products.component';

@NgModule({
  declarations: [
    ProductsComponent,
    ProductsListComponent,
    CreateProductPostComponent,
    EditComponent,
  ], //GamesComponent AND CreateGamePostComponent HAVE THE SAME SERVICE!!!!!!!!!!
  providers: [ProductsService],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
  ],
})
export class ProductsModule {}

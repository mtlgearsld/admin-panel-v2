import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGamePostComponent } from './create-game-post.component';

describe('CreateGamePostComponent', () => {
  let component: CreateGamePostComponent;
  let fixture: ComponentFixture<CreateGamePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGamePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGamePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

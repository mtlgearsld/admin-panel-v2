import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { CreateGamePostComponent } from './create/create-game-post.component';
import { EditComponent } from './edit/edit.component';
import { GamesRoutingModule } from './games-routing.module';
import { GamesComponent } from './games.component';
import { GamesService } from './games.service';
import { GamesListComponent } from './list/games.component';

@NgModule({
  declarations: [
    GamesComponent,
    GamesListComponent,
    CreateGamePostComponent,
    EditComponent
  ], //GamesComponent AND CreateGamePostComponent HAVE THE SAME SERVICE!!!!!!!!!!
  providers: [GamesService],
  imports: [
    CommonModule,
    GamesRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule
  ]
})
export class GamesModule {}

import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { GamesService } from '../games.service';
import { mergeMap, map, switchMap, take } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesListComponent {
  removedItems = new BehaviorSubject([]);
  removeItemFromSource = id =>
    this.removedItems.next([...this.removedItems.getValue(), id]);
  source = this.games
    .all()
    .pipe(
      mergeMap(data =>
        this.removedItems.pipe(
          map(ids => data.filter(d => !ids.includes(d._id)))
        )
      )
    );
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      mode: 'external'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      mode: 'external'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      // _id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false
      // },
      title: {
        title: 'Title',
        type: 'string'
      },
      developer: {
        title: 'Developer',
        type: 'string'
      },
      year: {
        title: 'Year',
        type: 'number'
      },
      description: {
        title: 'Description',
        type: 'number'
      },
      imageCover: {
        title: 'ImageCover',
        type: 'string'
      },
      images: {
        title: 'Images',
        type: 'string'
      }
    }
  };

  constructor(private games: GamesService, private router: Router) {}

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.games
        .remove(event.data._id)
        .pipe(take(1))
        .subscribe(() => this.removeItemFromSource(event.data._id));
    }
  }

  onEdit(event): void {
    this.router.navigate(['/pages/games/edit', event.data._id]);
  }

  onCreate(event): any {
    this.router.navigate(['pages', 'games', 'create', event]);
  }
}

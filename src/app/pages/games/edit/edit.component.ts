import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { GamesService } from '../games.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {
  subs = new Subscription(); //this.subs.add()
  gameForm = new FormGroup({
    title: new FormControl(''),
    developer: new FormControl(''),
    year: new FormControl(''),
    description: new FormControl(''),
    summary: new FormControl(''),
    imageCover: new FormControl(''),
    images: new FormControl('')
  });

  constructor(
    private games: GamesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .pipe(switchMap(params => this.games.getOne(params.get('id')))) //use switchMap to kill other request and take the last request
        .subscribe((game: any) => {
          {
            const { images, imageCover, ...gameRest } = game; //you have to send the images seperatly to do so pass images and imageCover on thier own and then the rest of the data
            console.log(game);
            this.gameForm.patchValue(gameRest);
            // this.gameForm.patchValue(images);
            // this.gameForm.patchValue(imageCover);
          }
        })
    ); //patchValue to prefill the form
  }

  //when the form loads use getOne() to prefill the form
  onSubmit() {
    //when you submit updateOne()s
    this.games
      .update(this.route.snapshot.params.id, this.gameForm.value) //snapshot will take id from url
      .subscribe(() => this.router.navigateByUrl('games'));
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}

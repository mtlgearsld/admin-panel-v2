import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { MoviesService } from '../../@core/mock/movies.service';
import { ThemeModule } from '../../@theme/theme.module';
import { CreateMoviePostComponent } from './create/create-movie-post.component';
import { EditMoviesComponent } from './edit-movies/edit-movies.component';
import { MoviesListComponent } from './list/movies.component';
import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';

@NgModule({
  declarations: [
    MoviesComponent,
    MoviesListComponent,
    CreateMoviePostComponent,
    EditMoviesComponent
  ], //moviesComponent AND CreateGamePostComponent HAVE THE SAME SERVICE!!!!!!!!!!
  providers: [MoviesService],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule
  ]
})
export class MoviesModule {}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { environment } from '../../environments/environment';

// const { apiUrl } = environment;

const apiUrl = (path: string) => `http://localhost:3000/${path}`;

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  constructor(private http: HttpClient) {}
  all() {
    return this.http.get<any[]>(apiUrl('movies'));
  }

  getOne(id: string) {
    return this.http.get(apiUrl(`movies/${id}`));
  }

  create(movie: any) {
    return this.http.post(apiUrl(`movies/`), movie);
  }

  update(id: string, movie: any) {
    return this.http.patch(apiUrl(`movies/${id}`), movie); //we need two params the id and the body
  }

  remove(id: string) {
    return this.http.delete(apiUrl(`movies/${id}`));
  }
}

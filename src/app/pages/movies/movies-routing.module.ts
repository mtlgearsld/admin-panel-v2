import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateMoviePostComponent } from './create/create-movie-post.component';
import { EditMoviesComponent } from './edit-movies/edit-movies.component';
import { MoviesListComponent } from './list/movies.component';
import { MoviesComponent } from './movies.component';

const routes: Routes = [
  {
    path: '',
    component: MoviesComponent,
    children: [
      { path: 'list', component: MoviesListComponent },
      { path: 'create', component: CreateMoviePostComponent }, //do it like this for now so you can see where the routes are going but later ask bro how to do child routes
      { path: 'edit/:id', component: EditMoviesComponent }, //dont forget the COLON in the /:id!!!!!!
      { path: '', redirectTo: 'list', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule {}

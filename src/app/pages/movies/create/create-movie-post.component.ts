import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder } from '@angular/forms';

import { MoviesService } from '../movies.service';
import { Router } from '@angular/router';
import { isArray } from 'util';

@Component({
  selector: 'app-create-movie-post',
  templateUrl: './create-movie-post.component.html',
  styleUrls: ['./create-movie-post.component.scss']
})
export class CreateMoviePostComponent implements OnInit {
  poster: any;
  movieForm = new FormGroup({
    title: new FormControl(''),
    director: new FormControl(''),
    year: new FormControl(''),
    genre: new FormControl(''),
    imageCover: new FormControl(''),
    images: this.formBuilder.array([])
  });

  constructor(
    private movies: MoviesService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {}

  onSubmit() {

    console.log(this.movieForm.value);
    const images = this.photos.controls.map<FileList>(img => img.value.file);

    const data = { ...this.movieForm.value, images, imageCover: this.poster };

    const payload = new FormData();

    Object.keys(data).forEach(key =>
      isArray(data[key])
        ? data[key].forEach(file => payload.append(`${key}`, file))
        : payload.append(key, data[key])
    );

    this.movies
      .create(payload)
      .subscribe(() => this.router.navigateByUrl('/pages'));
      console.log(payload);
    //You made a huge mistake before you were not using the same names in the FormGroup as you Server (i.e: you had gameTitle but in the Server you wrote 'title' so when you tried to write your serve didnt know what the form was doing)
  }
  get photos(): FormArray {
    return this.movieForm.get('images') as FormArray;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.poster = file;
    }
  }

  detectFiles(event) {
    const files = event.target.files;

    if (files) {
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.photos.push(
            this.formBuilder.group({ file, url: e.target.result })
          );
        };
        reader.readAsDataURL(file);
      }
    }
  }
}

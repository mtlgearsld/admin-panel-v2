import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMoviePostComponent } from './create-movie-post.component';

describe('CreateMoviePostComponent', () => {
  let component: CreateMoviePostComponent;
  let fixture: ComponentFixture<CreateMoviePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMoviePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMoviePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from '@angular/core';

@Component({
  selector: 'app-movies',
  template: `
    <router-outlet> </router-outlet>
  `
})
export class MoviesComponent {}

//we need this file because it is the base route that houses all the children

import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { MoviesService } from '../movies.service';
import { mergeMap, map, switchMap, take } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesListComponent {
  removedItems = new BehaviorSubject([]);
  removeItemFromSource = id =>
    this.removedItems.next([...this.removedItems.getValue(), id]);
  source = this.movies
    .all()
    .pipe(
      mergeMap(data =>
        this.removedItems.pipe(
          map(ids => data.filter(d => !ids.includes(d._id)))
        )
      )
    );
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      mode: 'external'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      mode: 'external'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      // _id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false
      // },
      title: {
        title: 'Title',
        type: 'string'
      },
      director: {
        title: 'Director',
        type: 'string'
      },
      year: {
        title: 'Year',
        type: 'number'
      },
      genre: {
        title: 'Genre',
        type: 'string'
      },
      imageCover: {
        title: 'ImageCover',
        type: 'string'
      },
      images: {
        title: 'Images',
        type: 'string'
      }
    }
  };

  constructor(private movies: MoviesService, private router: Router) {}

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.movies
        .remove(event.data._id)
        .pipe(take(1))
        .subscribe(() => this.removeItemFromSource(event.data._id));
    }
  }

  onEdit(event): void {
    this.router.navigate(['/pages/movies/edit', event.data._id]);
  }

  onCreate(event): any {
    this.router.navigate(['pages', 'movies', 'create', event]);
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-edit-movies',
  templateUrl: './edit-movies.component.html',
  styleUrls: ['./edit-movies.component.scss']
})
export class EditMoviesComponent implements OnInit, OnDestroy {
  subs = new Subscription(); //this.subs.add()
  movieForm = new FormGroup ({
    title: new FormControl(''),
    director: new FormControl(''),
    year: new FormControl(''),
    genre: new FormControl(''),
    imageCover: new FormControl('')
  }) 

  constructor(
    private movies: MoviesService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap
    .pipe(
      switchMap(params => this.movies.getOne(params.get('id')))
    ).subscribe((movie: any) => {
      {
        const { images, imageCover, ...movieRest } = movie;
        this.movieForm.patchValue(movieRest)
        console.log(movie);
      }
    })
  }

  onSubmit() {
    this.movies
      .update(this.route.snapshot.params.id, this.movieForm.value) //snapshot will take id from url
      .subscribe(() => this.router.navigateByUrl('movies'));
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.unsubscribe()
  }
}

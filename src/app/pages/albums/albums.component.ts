import { Component } from '@angular/core';

@Component({
  selector: 'app-albums',
  template: `
    <router-outlet> </router-outlet>
  `
})
export class AlbumsComponent {}

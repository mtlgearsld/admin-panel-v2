import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { AlbumsService } from '../albums.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-albums',
  templateUrl: './edit-albums.component.html',
  styleUrls: ['./edit-albums.component.scss']
})
export class EditAlbumsComponent implements OnInit {
  subs = new Subscription();
  albumForm = new FormGroup({
    artist: new FormControl(''),
    genre: new FormControl(''),
    yearActive: new FormControl(''),
    album: new FormControl(''),
    description: new FormControl(''),
    imageCover: new FormControl(''),
    images: new FormControl('')
  });

  constructor(
    private albums: AlbumsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .pipe(switchMap(params => this.albums.getOne(params.get('id')))) //use switchMap to kill other request and take the last request
        .subscribe((album: any) => {
          {
            const { images, imageCover, ...albumRest } = album; //you have to send the images seperatly to do so pass images and imageCover on thier own and then the rest of the data
            console.log(album);
            this.albumForm.patchValue(albumRest);
          }
        })
    );
  }

  onSubmit() {
    this.albums
      .update(this.route.snapshot.params.id, this.albumForm.value) //snapshot will take id from url
      .subscribe(() => this.router.navigateByUrl('albums'));
  }

  ngOnDestroy() {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.unsubscribe();
  }
}

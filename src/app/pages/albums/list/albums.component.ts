import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlbumsService } from '../albums.service';
import { mergeMap, map, switchMap, take } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsListComponent {
  removedItems = new BehaviorSubject([]);
  removeItemFromSource = id =>
    this.removedItems.next([...this.removedItems.getValue(), id]);
  source = this.albums
    .all()
    .pipe(
      mergeMap(data =>
        this.removedItems.pipe(
          map(ids => data.filter(d => !ids.includes(d._id)))
        )
      )
    );
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      // _id: {
      //   title: 'ID',
      //   type: 'number',
      //   editable: false
      // },
      artist: {
        title: 'Artist',
        type: 'string'
      },
      genre: {
        title: 'Genre',
        type: 'string'
      },
      yearActive: {
        title: 'Year Active',
        type: 'number'
      },
      album: {
        title: 'Album',
        type: 'string'
      },
      description: {
        title: 'Description',
        type: 'string'
      },
      imageCover: {
        title: 'ImageCover',
        type: 'string'
      },
      images: {
        title: 'Images',
        type: 'string'
      }
    }
  };

  constructor(private albums: AlbumsService, private router: Router) {}

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.albums
        .remove(event.data._id)
        .pipe(take(1))
        .subscribe(() => this.removeItemFromSource(event.data._id));
    }
  }

  onEdit(event): void {
    this.router.navigate(['/pages/albums/edit', event.data._id]);
  }

  onCreate(event): any {
    this.router.navigate(['pages', 'albums', 'create', event]);
  }
}

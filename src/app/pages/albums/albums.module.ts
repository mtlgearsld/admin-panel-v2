import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { AlbumsRoutingModule } from './albums-routing.module';
import { AlbumsComponent } from './albums.component';
import { AlbumsService } from './albums.service';
import { CreateAlbumPostComponent } from './create/create-album-post.component';
import { EditAlbumsComponent } from './edit-albums/edit-albums.component';
import { AlbumsListComponent } from './list/albums.component';

@NgModule({
  declarations: [
    AlbumsComponent,
    AlbumsListComponent,
    CreateAlbumPostComponent,
    EditAlbumsComponent
  ],
  providers: [AlbumsService],
  imports: [
    CommonModule,
    AlbumsRoutingModule,
    ReactiveFormsModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule
  ]
})
export class AlbumsModule {}

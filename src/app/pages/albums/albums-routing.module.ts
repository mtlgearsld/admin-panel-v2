import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlbumsComponent } from './albums.component';
import { CreateAlbumPostComponent } from './create/create-album-post.component';
import { EditAlbumsComponent } from './edit-albums/edit-albums.component';
import { AlbumsListComponent } from './list/albums.component';

const routes: Routes = [
  {
    path: '',
    component: AlbumsComponent,
    children: [
      { path: 'list', component: AlbumsListComponent },
      { path: 'create', component: CreateAlbumPostComponent },
      { path: 'edit/:id', component: EditAlbumsComponent },
      { path: '', redirectTo: 'list', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumsRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { AlbumsService } from '../albums.service';
import { isArray } from 'util';

@Component({
  selector: 'app-create-album-post',
  templateUrl: './create-album-post.component.html',
  styleUrls: ['./create-album-post.component.scss']
})
export class CreateAlbumPostComponent implements OnInit {
  poster: any;
  albumForm = new FormGroup({
    artist: new FormControl(''),
    genre: new FormControl(''),
    yearActive: new FormControl(''),
    album: new FormControl(''),
    description: new FormControl(''),
    imageCover: new FormControl(''),
    images: this.formBuilder.array([])
  });

  constructor(
    private albums: AlbumsService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {}
  onSubmit() {
    const images = this.photos.controls.map<FileList>(img => img.value.file);

    const data = { ...this.albumForm.value, images, imageCover: this.poster };

    const payload = new FormData();

    Object.keys(data).forEach(key =>
      isArray(data[key])
        ? data[key].forEach(file => payload.append(`${key}`, file))
        : payload.append(key, data[key])
    );

    this.albums
      .create(payload)
      .subscribe(() => this.router.navigateByUrl('/pages')); //You made a huge mistake before you were not using the same names in the FormGroup as you Server (i.e: you had gameTitle but in the Server you wrote 'title' so when you tried to write your serve didnt know what the form was doing)
  } //Also there was no need for (http: HttpClient) noooooo just use private games: GamesService because you will later subscribe to this Service
  // Help to get all photos controls as form array.
  get photos(): FormArray {
    return this.albumForm.get('images') as FormArray;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.poster = file;
    }
  }

  detectFiles(event) {
    const files = event.target.files;

    if (files) {
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.photos.push(
            this.formBuilder.group({ file, url: e.target.result })
          );
        };
        reader.readAsDataURL(file);
      }
    }
  }
}

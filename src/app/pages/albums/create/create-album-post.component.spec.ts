import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAlbumPostComponent } from './create-album-post.component';

describe('CreateAlbumPostComponent', () => {
  let component: CreateAlbumPostComponent;
  let fixture: ComponentFixture<CreateAlbumPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAlbumPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAlbumPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

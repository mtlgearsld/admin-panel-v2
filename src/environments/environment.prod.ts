// /**
//  * @license
//  * Copyright Akveo. All Rights Reserved.
//  * Licensed under the MIT License. See License.txt in the project root for license information.
//  */
// export const environment = {
//   production: true,
// };
import { environment as dev } from './environment';

export const environment = {
  ...dev, // an object spread to get everything from env.ts in here
  production: true
};
